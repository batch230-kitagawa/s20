//console.log("Hello World");

/*
S20 Instructions

1. In the S20 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable number that will store the value of the number provided by the user via the prompt.
4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
5. Create a condition that if the current value is less than or equal to 50, stop the loop.
6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7. Create another condition that if the current value is divisible by 5, print the number.
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10.Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.
13. Create a git repository named S20.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.

*/

let number = Number(prompt("Enter a number."));
	console.log("The number you provided is " + number + ".");

for(let i = number; i > 0; i--){
	if(i <= 50){
		break;
	}
	else if(i % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}
	else if(i % 5 == 0){
		console.log(i);
	}
}

let string1 = "supercalifragilisticexpialidocious";
let string2 = "";

for(let i=0; i<string1.length; i++){
	if(string1[i].toLowerCase() == 'a' || string1[i].toLowerCase() == 'e' || string1[i].toLowerCase() == 'i' || string1[i].toLowerCase() == 'o' || string1[i].toLowerCase() == 'u' ){
		continue;
	}
	else{
		string2 = string2 + string1[i];
	}
}
console.log(string1);
console.log(string2);

